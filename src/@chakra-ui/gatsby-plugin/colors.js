export const colors = {
  primary: '#212b44',
  secondary: '#ff5500',
  white: '#ffffff',
  gray: '#262220',
};
