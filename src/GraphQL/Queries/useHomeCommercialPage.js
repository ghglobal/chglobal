import { graphql, useStaticQuery } from 'gatsby';

function useHomeCommercialPage() {
  const data = useStaticQuery(
    graphql`
      query HomeCommercialPageQuery {
        wpgraphql {
          pageBy(uri: "home-commercial") {
            acf_hero {
              fieldGroupName
              sliderTitle
              sliderImages {
                image {
                  altText
                  sourceUrl
                }
              }
            }

            acf_text_block_title {
              title
              content
              hastTwoBlocks
              secondTitle
              secondContent
            }

            acf_testimonials {
              mainTitle
              mainContent
              image {
                altText
                sourceUrl
              }
            }

            acf_contact_cta {
              contactCtaTitle
              contactCtaContent
              contactCtaButton {
                url
                title
              }
              contactCtaBackgroundImage {
                altText
                sourceUrl
              }
            }
          }

          services(where: { orderby: { field: SLUG, order: ASC } }) {
            nodes {
              title
              excerpt
              acf_service_info {
                serviceUrl
                icon {
                  sourceUrl
                }
                background {
                  sourceUrl
                }
              }
            }
          }
        }
      }
    `,
  );
  return data.wpgraphql;
}

export { useHomeCommercialPage };
