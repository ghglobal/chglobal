import { graphql, useStaticQuery } from 'gatsby';

function usePaitingService() {
  const data = useStaticQuery(
    graphql`
      query PaintingServiceQuery {
        wpgraphql {
          serviceBy(serviceId: 159) {
            acf_service_info {
              serviceTitle
              serviceShortDescription
              serviceUrl
              background {
                sourceUrl
              }
              icon {
                sourceUrl
              }
            }
            acf_text_block_title {
              title
              content
            }
            acf_image_text_block {
              blockList {
                title
                content
                image {
                  sourceUrl
                }
              }
            }
            acf_services_block {
              servicesList {
                serviceTitle
                serviceDescription
                mainImage {
                  sourceUrl
                }
              }
            }
            acf_projects_slider {
              projectList {
                projectName
                projectLocation
                mainImage {
                  sourceUrl
                }
                projectGallery {
                  sourceUrl
                }
              }
            }
            acf_quote {
              descriptionQuote
              phoneNumber
              titleQuoteSection
              iconQuote {
                sourceUrl
              }
              backgroundImage {
                sourceUrl
              }
            }
          }
        }
      }
    `,
  );
  return data.wpgraphql;
}

export { usePaitingService };
