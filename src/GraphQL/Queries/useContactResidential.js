import { graphql, useStaticQuery } from 'gatsby';

function useContactResidentialPage() {
  const data = useStaticQuery(
    graphql`
      query ContactResidentialPageQuery {
        wpgraphql {
          pageBy(uri: "contact-residential") {
            acf_hero {
              heroTitle
              heroDescription
              heroImage {
                sourceUrl
              }
            }
            acf_map_location {
              locationUrl
            }
            acf_contact_information {
              address
              email
              phoneNumber
            }
          }
        }
      }
    `,
  );
  return data.wpgraphql;
}

export { useContactResidentialPage };
