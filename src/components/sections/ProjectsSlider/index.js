import React from 'react';
import { ProjectCarousel } from '../../shared';

export const ProjectsSection = ({ projects, residential }) => {
  return <ProjectCarousel projects={projects} isResidential={!!residential} />;
};
