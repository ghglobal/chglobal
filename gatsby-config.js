require('isomorphic-unfetch');
const path = require('path');
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: `CH Global`,
    description: `CH Global is a construction company that delivers reliable and trusted services for your home or business.`,
    author: `CHGLobal`,
    siteUrl: `${process.env.GATSBY_SITE_URL}/`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sass`,
    {
      resolve: '@chakra-ui/gatsby-plugin',
      options: {
        isResettingCSS: true,
        isUsingColorMode: false,
      },
    },
    {
      resolve: 'gatsby-source-graphql',
      options: {
        typeName: 'WPGraphQL',
        fieldName: 'wpgraphql',
        url: `${process.env.GATSBY_WORDPRESS_URL}/graphql`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `CHGlobal`,
        short_name: `chglobal`,
        start_url: `/`,
        background_color: `#212b44`,
        theme_color: `#212b44`,
        display: `minimal-ui`,
        icon: `static/images/white-logo.png`, // This path is relative to the root of the site.
      },
    },
  ],
};
